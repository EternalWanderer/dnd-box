# D&D Box

For some baffling reason, the zsh completions of my [sheet-parser](https://gitlab.com/EternalWanderer/sheet-parser) refuse to work on my main laptop.
I have no idea what's causing it.
As a workaround, I tossed both it and the [dice-roller](https://gitlab.com/EternalWanderer/dice-roller) in a docker container.
This also has the benifit of it now _technically_ being cross platform.

## Building

Run `make` in the repository and things should automatically work.

If you are on an OS that doesn't support GNU Make or don't want to use GNU Make for some reason, you can use the following command to build the container.
```sh
docker build -t voidcruiser/dnd-box .
```

## Usage

To use with your own character sheets based on the example included with the sheet-parser mount a directory to `/characters` inside the container.

```sh
docker run -tiv </path/to/directory/with/sheets>:/characters --rm voidcruiser/dnd-box
```
