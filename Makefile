all: docker

docker:
	docker build -t voidcruiser/dnd-box .

clean:
	docker build --no-cache -t voidcruiser/dnd-box .

.PHONY: all docker clean
