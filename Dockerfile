FROM alpine:3.16
RUN wget -qO /etc/apk/keys/alpine@voidcruiser.nl.rsa.pub https://voidcruiser.nl/keys/alpine@voidcruiser.nl.rsa.pub
RUN echo https://alpine.voidcruiser.nl/stable >> /etc/apk/repositories
RUN apk -U upgrade
RUN apk add dice-roller dice-roller-doc sheet-parser sheet-parser-doc mandoc zsh zsh-syntax-highlighting fzf fzf-zsh-plugin coreutils
RUN adduser -h /player -s /bin/zsh player -D
COPY src /player
RUN mkdir /characters
RUN chown -R player:player /player /characters
USER player
WORKDIR /characters
CMD zsh
